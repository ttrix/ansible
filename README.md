# What is Ansible

Ansible is a tool to automate IT tasks like:

- Configure systems
- Deploy software
- Orchestrate more advanced IT tasks

# Use cases for Ansible

For repetitive tasks like:

- Updates
- Backups
- Creating Users and Assigning permissions
- Reboots

When you need the same configuration on many servers. Update all the servers at the same time with Ansible

# Ansible advantages

- Instead of SSHing into all remote server, execute tasks from your own machine
- Configuration / Installation / Deployment steps in a single file
- Re-user same file multiple times for different environments
- More reliable and less error prone
- Support all infrastructure from OS to cloud providers

# How Ansible works

## Ansible uses YAML

- YAML is used, because it's easier for humans to read and write. So, no need to learn a specific language because it is super intuitive.
- SO, no need to learn a specific language for Ansible

## Ansible is agentless

Ansible connects to remote servers using simple SSH, no special agent required

-> Your laptop connect using SSH to each remote server (cloud or bare-metal server), so no need to install Ansible in remote server since SSH is built-in all OS.

# Ansible modules

- Modules are small programs that do the actual work
- A module is a reusable, standalone script that Ansible runs on your behalf.
- Modules are VERY granular, performing a specific task. Modules for ONE VERY specific tasks.
- Get pushed to the target server, do their work and get removed
- You can develop your own modules or use existing ones, for example "jenkins_job" module

Example of modules for: create cloud instances, start docker container, start nginx server, install nginx server, create or copy file ...

**jenkins_job module**

```yaml
# Create a jenkins job using basic authentification
- jenkins_job:
    config: "{{ lookup('file', 'templates/test.xml') }}"
    name: test
    password: admin
    url: http://localhost:8080
    user: admin

# Delete a jenkins job using the token
- jenkins_job:
    name: test
    token: qsdfqsdfqfdssdfdqqsdfdsqfds
    state: absent
    url: http://localhost:8080
    user: admin
```

**docker_container module**

```yaml
# Create container
- name: Create a data container
  docker_container:
    name: mydata
    image: busybox
    volumes:
      - /data

# Start container
- name: Start a container with a command
  docker_container:
    name: sleepy
    image: ubuntu:14.04
    command: ["sleep", "infinity"]

# Apply some configuration
- name: Add container to networks
  docker_container:
    name: sleepy
    networks:
      - name: TestingNet
        ipv4_address: 172.1.1.18
        links:
          - sleepers
      - name: TestingNet2
        ipv4_address: 172.1.10.20
```

**postgresql module**

```yaml
# Create table
- name: Rename table foo to bar
  postgresl_table:
    table: foo
    rename: bar

# Set user privilege
- name: Set owner to someuser
  postgresql_table:
    name: foo
    owner: someuser

- name: Truncate table foo
  postgresql_table:
    name: foo
    truncate: yes
```

# Ansible playbooks = 1 or more Plays

- Put multiple modules together to accomplish a complex task
- Follow a specific sequence
- Describes

  - How and in which order
  - At what time and where (on which machines)
  - What (the modules) should be executed

- Play = Define which tasks, which hosts, which user
- host attribute refers to the Ansible inventory list defined in file hosts (it could be next to the playbook.yaml file or in a dedicated location)
- **hosts** file = Ansible inventory
- inventory = All the machines involved in task executions

Example of a playbook with multiple plays:

```yaml
#  Play for host group "webservers"
- name: Install and start nginx server

  hosts: webservers # where should these tasks be executed? which hosts?
  remote_user: root # which user should the tasks execute with?

  tasks: # which set of tasks/modules
    - name: create directory for nginx
      file:
        path: /path/to/nginx/dir
        state: directory

    - name: install nginx latest version
      yum:
        name: nginx
        state: latest

    - name: start nginx
      service:
        name: nginx
        state: started

# Play for host group "databases"
- name: rename table, set owner and truncate it
  hosts: databases # where should these tasks be executed? which hosts?
  remote_user: root # which user should the tasks execute with?
  vars: # using variables
    tablename: foo
    tableowner: someuser

  tasks:
    - name: Rename table {{ tablename }} to bar
      postgresl_table:
        table: "{{ tablename }}"
        rename: bar

    # Set user privilege
    - name: Set owner to someuser
      postgresql_table:
        name: "{{ tablename }}"
        owner: "{{ tableowner }}"

    - name: Truncate table {{ tablename }}
      postgresql_table:
        name: "{{ tablename }}"
        truncate: yes
```

Example of hosts file

```yaml
10.24.0.100

[webservers]        # it refereces/groups multiple IP addresses or host names
10.24.0.1           # cloud servers, virtual or bare metal servers
10.24.0.2

[databases]
10.24.0.7
10.24.0.8
```

# Comparable tools

## Ansible

- Simple YAML
- Agentless, only needs ssh access

## Puppet and Chef

- Use Ruby language, more difficult to learn
- Installation needed
- Need for managing updates on target servers

## Differences between Python and Ansible

- In Python WE need to check the status
- Ansible and Terraform handles that stat check for us
- Ansible is easier to write, because it's just YAML
- No programming skills needed

# Install Ansible

We can install Ansible either on our **local machine** or on a **remote server**.
The machine that runs Ansivlz is called the "Control Node"
Control Node manages the target servers
Windows is not supported for the control node

## Prerequisites

Ansible needs Python to run
