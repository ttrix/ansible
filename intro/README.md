# Cheat Sheet

## install Ansible in control node

Debian based linux: apt install ansible
Mac: brew install ansible

/!\ Python is a prerequisite for installing Ansible. If not installed, Python will be install along with ansible.

### installing Ansible using python package manager (pip)

```sh
pip install ansible
```

## Check ansible version

```sh
ansible --version
```

## Ad-hoc commands

Ad-hoc commands are not stored for future usage but it is a fast way to interact with the desired servers.

```sh
$ ansible [pattern] -m [module] -a "[module options]"
# [pattern] = targeting hosts and groups
```

### using 'all' the servers in hosts file

```sh
ansible all -i hosts -m ping
# "all" = default group, which contains every host
```

### targeting only servers in 'droplet' group

```sh
ansible droplet -i hosts -m ping
```

### using only a specific server by using its IP address

```sh
ansible 164.90.182.230 -i hosts -m ping
```

## Essential files

- hosts (invertory)
- ansible.cfg (ansible configuration)
- playbook.yml (ordered list of plays/actions and their tasks, plays & tasks runs in order from top to bottom)

## Ansible inventory file (hosts)

Default name: hosts

### Grouping hosts

We can put each host in more than one group
We can create groups that track:

- Where - a datacenter/region, e.g. east, west ...
- What - e.g. database servers, web servers, etc
- When - which stage, e.g. dev, test, prod environment ...

### Arguments/variables for a specific group of servers/hosts, e.g. droplet

```
[droplet:vars]
ansible_ssh_private_key_file=~/.ssh/id_rsa
ansible_user=root
```

### Set the python interpreter to be used by default

Add in hosts file either

- in front of server hostname/IP address
- vars section if working with groups (preferred)

```
ansible_python_interpreter=/usr/bin/python3
```

## Ansible configuration file (ansible.cfg)

Ansible supports several sources for configurin its behaviour, including an ini file named "ansible.cfg"

We can configure the file globally or for each projject, by creatin ansible.cfg file in the root folder of the project.

```sh
[defaults]
# Disabling ssh key checking by default, INSECURE
host_key_checking = False
# Set a default hosts file
inventory = hosts
```

### Set a default hosts file

Add the line below to the ansible.cfg file:

```
inventory = <%path_to_the_hosts_file%>
```

\*path_to_the_hosts_file <= path from the location of the directory containig the ansible.cfg file

### Default location for ansible.cfg file

Changes can be made and used in a configuration file which will be search for in the following order:

- ANSIBLE_CONFIG (environment variable if set)
- ansible.cfg (in the current directory)
- ~/.ansible.cfg (in the home directory)
- /etc/ansible/ansible.cfg

Ansible will process the above list and use the first file found, all others are ignored.

### Disabling ssh key checking, so no asking for validation of SSH key when connecting to the server for the 1st time

Example: 1st time connecting using ssh we need to confirm adding the server key into our ~/.ssh/known_hosts file

```sh
ssh root@64.226.120.221
The authenticity of host '64.226.120.221 (64.226.120.221)' can't be established.
ED25519 key fingerprint is SHA256:DiOcBW18ShnpW50pp/GkvWWdK00prRcEKV9rfTCOu4Q.
This key is not known by any other names.
Are you sure you want to continue connecting (yes/no/[fingerprint])?
```

#### Recommended method if "stable" infrastructure (the servers our long-live servers)

- add ssh-key from remote hosts to local known_hosts file

```sh
ssh-keyscan -H <%remote host IP%> >> ~/.ssh/known_hosts
```

- store our public ssh key into the server authorized_keys file (/root/.ssh/authorized_keys)

```sh
ssh-copy-id root@<%server IP address%>
```

#### INSECURE/LESS SECURE method - disabling by default

Useful when we have an ephemeral infrastructure (servers are dynamically created and destroyed). Insecure but safe as we are using a short-list of servers.

Add this to ansible.cfg file

```
[defaults]
host_key_checking = False
```

If the config file is located in home directory, it MUST be named .ansible.cfg (with period at the beggining)

## Ansible playbooks (playbook.yaml)

- A playbook can have multiple plays
- Ordered list of tasks
- A play is a group of ordered tasks
- Play & tasks run in order from top to bottom
- YAML format
- very first line contains only "---"

Ansible is idempotency: If I execute the same configuration multiple times, I will get the same results without new actions.

### Command to run the playbook

```sh
$ ansible-playbook -i hosts my-playbook.yaml
```

A module is a part of a collection.

## Ansible collections

A packaging format for bunding and distributing Ansible content
Can be released and installed independent of other collections

A collection can contain:

- Playbooks
- Plugins
- Modules

Instead of having it separately, we can pull all of these together and packaging in a single bundle, so a collection. Easy to distribute and share with all the ansible content inside.
For example: The modules 'apt' and 'service' are actually part of the same collection 'ansible.builtin'

## Ansible plugins

- Pieces of code that add to Ansible's functionality or modules
- We can also write our own Plugin

## Ansible galaxy (ansible repository)

Similar concept to repository like maven/docker repo

ansible-galaxy command allows us to fetch collections and more

### List all the collections installed in our machine

```sh
$ ansible-galaxy collection list
```

### Upgrade a specific collection (no need to upgrade the whole ansible installation)

```sh
$ ansible-galaxy collection install amazon.aws --upgrade
```

### Create own collection

- For bigger ansible projects
- Collections follow a simple data structue
- Required: a galaxy.yml file (containing metadata) at the root level of the collection

collection/
-- docs/
-- galaxy.yml
-- meta/..
-- plugins/..
-- roles/..
-- playbooks/..
-- tests/

## Ansible namespaces

e.g. community.docker.docker_image module

<namespace>.<collecion>.<module_name>

FQCN (Fully Qualified Collection Name) for docker_module = community.docker.docker_image
FQCN specifies the exact source of a module/plusgin/...

ansible.builtin = **default** namespace and collection name

## Use variables into plays

Variables should be inside double curly braces {{}}

```
- name: Create new linux user for nodejs
  hosts: webserver
  tasks:
    - name: Create linux user
      user:
        name: david
        comment: nodejs admin
        group: admin
      register: user_creation_result  # create variable and send result of command to
    - debug: msg={{user_creation_result}}
```

but if the variable is used just after colon (:), we should double quotes around THE WHOLE LINE/VALUE

```
- name: Install npm dependencies
  npm:
  path: "{{destination}}/package"
```

## Privilege escalation: become

- become_user = set to user with desired privileges. Default is root
- become = allows us to 'become' another user, **different from the user that logged into the machine**
