# Using variables

Variable can be ued to parameterize our Playbook to make it customizable
So we can use the same Ansible script for different environments by substituting some dynamic values

## Registering variables

With "register" we can create variables from the output of an Ansible task
This variable can ve used in any later task in our Play

```yaml
  ...
  - name: Ensure app is running
    ansible.builtin.shell: ps aux | grep node
    register: app_status
  - ansible.builtin.debug: msg={{app_status.stdout_lines}}
  ...
```

## Referencing variables

Using **doble curly braces**
If curly braces {{ value }} directly after attribute, we must quote the whole expression to create valid YAML syntax

```yaml
- name: Start the application
  ansible.builtin.command:
    chdir: "{{destination}}/package/app"
```

## Naming of variables

**Wrong**: Playbook keywords, such as environment
**Valid**: Letters, numbers and underscores
Should alwas start with letter
**Wrong**: linux-name, linux name, linux. or 12
**Valid**: linux_name

## Setting vars directly in the playbook

Usage:

```yaml
  ...
  vars:
    location: ../node-app
    version: 1.0.0
    destination: /home/david
  tasks:
    - name: Copy nodejs tar file to server and unpack it
      ansible.builtin.unarchive:
        src: "{{location}}/nodejs-app-{{version}}.tgz"
        dest: "{{destination}}"
```

## Using external configuration files (BEST WAY)

Variable files uses YAML syntax

```yaml
version: 1.0.0
app_location: ../node-app
linux_name: david
user_home_dir: /home/{{linux_name}}
```

Usage:

We need to add this in every set of tasks

```yaml
  ...
  vars_files:
    project-vars
  ...
```

## Setting values on Command Line

Command line wil be like this:

```sh
ansible-playbook -i hosts deploy-node.yaml --extra-vars "version=1.0.0 location=../node-app linux_name=david"
# OR
ansible-playbook -i hosts deploy-node.yaml -e "version=1.0.0 location=../node-app linux_name=david"
```

## Examples

Check: ../projects/02-deploy_nodejs_app

## Variable precedence

Here is the order of precedence from least to greatest (the last listed variables override all other variables):

1. command line values (for example, -u my_user, these are not variables)
2. role defaults (defined in role/defaults/main.yml)
3. inventory file or script group vars
4. inventory group_vars/all
5. playbook group_vars/all
6. inventory group_vars/\*
7. playbook group_vars/\*
8. inventory file or script host vars
9. inventory host_vars/\*
10. playbook host_vars/\*
11. host facts / cached set_facts
12. play vars
13. play vars_prompt
14. play vars_files
15. role vars (as defined in Role directory structure)
16. block vars (only for tasks in block)
17. task vars (only for the task)
18. include_vars
19. set_facts / registered vars
20. role (and include_role) params
21. include params
22. extra vars (for example, -e "user=my_user")(always win precedence)
