# Notes about AAP (Ansible Automation Platform)

Web-based solution that makes Ansible more easy to use.

## AAP vs AWX vs CLI

AAP: Web-based solution. Subscription required.
AWX: Web-based solution. Open-source.
CLI: Command Line Interface = Terminal

## Order when creating a job to run

- Credentials SCM (Source Control Management - ie. Git)
- Machine creentials
- Inventory
- Projects
- Templates
- Jobs

## extra variables

```
-e "var1=value1 vars2=value2"
```

This is called "Survey" in AAP

## sudo privilege

add this when defining the task

```
become: yes
```

This is called "Privilege Escalation" in AAP
