# Ansible Roles

We can group our content in roles to easily reuse and share them with other users
Break up large Playbooks into smaller manageable files

Roles are like a package for our tasks (including files, vars, modules needed for the tasks)
Roles are like small applications
Easy to navigate, maintain and reuse
Develop and test separately because of the separate

## Roles can contain

- Tasks: that the role executes
- Static files: that the role deployes
- (Default) variables: for the tasks (which we can overwrite)
- Custom modules, which are used within this role

## Standard file structure

A role has a defined directory structure
So, it's easy to navigate and maintain

```sh
# playbooks
site.yaml
webservers.yaml
fooservers.yaml
roles/
    common/
        tasks/main.yaml
        handlers/
        vars/main.yaml          # specific vars (or values) depending on environment for example
        library/my_module.py
        files/my-file.yaml
        templates/
        vars/
        defaults/main.yaml      # default vars used in the role
        meta/
    webserver
        tasks/
        defaults/
        meta/
```

### tasks/main.yaml

It stores the tasks used in the role

### handlers/

### library/my_module.py

### files/my-file.yaml

Files to be transferred/used in the roles

### templates/

### vars/main.yaml

specific vars (or values) depending on environment for example

### defaults/main.yaml

Default values for vars used in the role

- Parameterize role, but execute without having to define variables
- Possibility to overwrite default variables

### meta/

Metadata like role maintainers, copyright policy ...

## Use existing roles

write own roles or use existing ones from community
Download from Ansible Galaxy or Git repository

## Like functions

Extract common login
Use function/role in different places with different parameters

## Usage of roles in a play

```
# playbooks
my-playbook.yaml
roles/
  create_user
    defaults/
      main.yaml
    tasks/
      main.yaml
  start_containers
    defaults/
       main.yaml
    files/
      docker-compose.yaml
    tasks/
      main.yaml
    vars/
      main.yaml
```

my-playbook.yaml

```yaml
- name: Create new linux server
  hosts: all
  become: yes
  vars:
    user_groups: adm,docker
  roles:
    - create_user # name of the folder inside the 'roles' directory

- name: Start docker containers
  hosts: all
  become: yes
  become_user: user1
  vars_files:
    - project-vars
  roles:
    - start_containers # name of the folder inside the 'roles' directory
```

/!\ Ansible knows where to find the roles and tasks because of the specific structure and naming we have in roles folder

### Roles can access variables from playbook

The roles will use any variables / vars_files defined in the playbook calling it and those variables can also be overrided inside the role.

## Create a role

- Create a folder called 'roles' next to the playbook using the new role
- Inside de folder 'roles' run this command (this will create the file structure for the role):

```sh
$ ansible-galaxy init <%role name%>
```

## Example project

We will split the project 04-Run_docker_apps/deploy-to-AWS-EC2 into roles

- create_user
- start_container
- install_docker
- install_docker_compose
