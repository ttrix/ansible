# Notes

## Steps we will go through:

- Create a Droplet (DO)
- Write Ansible playbook
  - Install node and npm on server
  - Create node package (npm pack)
  - Copy Node artifact to the server and unpack it there
  - Start application using node command
  - Verify app runnin successfully

## command module

### add async args to avoid blocking

```
name: Start the application
command: node /root/package/app/server
async: 1000
poll: 0
```

command module is more secure than shell module (avoid shell injection)
