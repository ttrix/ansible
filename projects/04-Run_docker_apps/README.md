# Run Docker applications

## Overview

- Create an AWS EC2 instance with Terraform
- Configure inventory file to connect to AWS EC2 instance
- Install Docker and docker-comopse
- Copy docker-compose file to server
- Start Docker containers
