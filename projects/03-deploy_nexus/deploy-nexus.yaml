---
- name: install Java and net-tools
  hosts: nexus_server
  tasks:
    # apt update
    - name: Update apt repository
      apt: update_cache=yes force_apt_get=yes cache_valid_time=3600
    # apt install openjdk-8-jre-headless
    - name: Install Java 8
      apt: name=openjdk-8-jre-headless
    # apt install net-tools
    - name: Install net-tools
      apt: name=net-tools

- name: Download and unpack Nexus installer
  hosts: nexus_server
  tasks:
    # cd /opt
    - name: Check nexus folder stats
      stat:
        path: /opt/nexus
      register: stat_result

    # wget https://download.sonartype.com/nexus/3/latest-unix.tar.gz
    - name: Download Nexus
      get_url:
        url: https://download.sonatype.com/nexus/3/latest-unix.tar.gz
        dest: /opt/
      register: download_result
      when: not stat_result.stat.exists

    - debug: msg={{stat_result}}
    - debug: msg={{download_result}}

    # tar -zxvf latest-unix.tar.gz
    - name: Unpack Nexus
      unarchive:
        src: "{{download_result.dest}}"
        dest: /opt/
        remote_src: yes # source file is stored in server and not in local machine (ansible controller node)
      when: not stat_result.stat.exists
    - name: Find nexus folder
      find:
        paths: /opt/
        patterns: "nexus-*"
        file_type: directory
      register: find_result
    - name: Rename nexus folder
      shell: mv {{find_result.files[0].path}} /opt/nexus
      # we need a conditional here because for Shell and Command module,
      # Ansible cannot know the state of the task
      when: not stat_result.stat.exists

- name: Create nexus user to own nexus folders
  hosts: nexus_server
  tasks:
    # adduser nexus
    - name: Ensure nexus group exists
      group:
        name: nexus # specific group for working with nexus
        state: present
    - name: Create nexus user
      user:
        name: nexus # specific user for nexus installation
        group: nexus

    # chown -R nexus:nexus nexus-3.28.1-01
    - name: Make nexus user owner of nexus folder
      file:
        path: /opt/nexus
        state: directory
        owner: nexus
        group: nexus
        recurse: yes

    # chown -R nexus:nexus sonartype-work
    - name: Make nexus user owner of sonartype folder
      file:
        path: /opt/sonatype-work
        state: directory
        owner: nexus
        group: nexus
        recurse: yes

- name: Start nexus with nexus user
  hosts: nexus_server
  become: True
  # su - nexus
  become_user: nexus
  tasks:
    - name: Set run_as_user nexus
      lineinfile:
        # vim nexus/bin/nexus.rc
        path: /opt/nexus/bin/nexus.rc
        # run_as_user="nexus"
        regexp: '^#run_as_user=""'
        line: run_as_user="nexus"
      #blockinfile:
      #  path: /opt/nexus/bin/nexus.rc
      #  block: |
      #    run_as_user="nexus"

    # /opt/nexus/bin/nexus start
    - name: Start nexus
      command: /opt/nexus/bin/nexus start
      register: result_start_nexus
    - debug: msg={{result_start_nexus.stdout}}

- name: Verify nexus is running
  hosts: nexus_server
  tasks:
    # ps -aux | grep nexus
    - name: Check with ps
      shell: ps -aux | grep nexus
      register: app_status
    - debug: msg={{app_status.stdout_lines}}

    - name: Wait one minute
      pause: # alternative and more advanced option is "ansible.builtin.wait_for" module
        minutes: 1

    # netstat -lnpt
    - name: Check with netstat
      shell: netstat -lnpt
      register: app_status
    - debug: msg={{app_status.stdout_lines}}
