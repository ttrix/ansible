# Convert Shell script to Ansible playbook

In this project, we translate the install_nexus.sh shell script into deploy_nexus.yaml Ansible playbook

# useful

If issues when starting Nexus server we can run this command to see what is happening.

```sh
$ /opt/nexus/bin/nexus run
```

Output like: "Cannot allocate memory" meaning that we need more resources like RAM.
